# GitLab Feature Flags in Laravel

Use GitLab [feature flags](https://docs.gitlab.com/ee/operations/feature_flags.html) in your Laravel app.

## Getting Started

1. Set up feature flags in a project in your GitLab instance.
1. Install this library in your project.

   ```bash
   composer require codingpaws/gitlab-feature
   ```

1. Publish the configuration

   ```bash
   php artisan vendor:publish --provider=CodingPaws\\GitLabFeature\\FeatureServiceProvider
   ```

1. Go to your project’s feature flags and click on **Configure**  
   ![The configure button is next to the "View user lists" and "New feature flag" buttons](docs/images/configure-button.png)
1. Add entries to your `.env` config
   - `GITLAB_INSTANCE_ID` to the instance ID shown in the **Configure** modal
   - `GITLAB_API_URL` to the API url shown in the **Configure** modal
   - `GITLAB_ENV_NAME` to your environment name which you use in your feature flag rollout strategies.

## Usage in PHP

When you want to check a feature flag in PHP, such as controllers or models, you can use the `Feature` class:

```php
use CodingPaws\GitLabFeature\Feature;

if (Feature::enabled('colored_shopping_cart')) {
   echo '<div class="shopping-cart__colored" />';
}

if (Feature::disabled('date-format-24h')) {
   $dateFormat = DATE_FORMAT_AMPM;
}
```

## Usage in Blade

Importing in blade makes the views hard to read. In views you can use the provided helpers methods:

```php
@if(feature_enabled('new_design'))
   <link rel="stylesheet" href="/static/design-2021.css" />
@endif

@unless(feature_disabled('lazyload'))
   <script src="/js/lazyload.min.js" defer async></script>
@endif
```

## Feature flag cache

Feature flags are cached using [the Laravel `Cache` system][laravel-cache].
This improves your site’s performance and reduces the load on your GitLab
instance. The cache duration can be configured in your
`config/gitlab_feature.php` config file using the `cache_duration` entry. The
standard environment variable is `GITLAB_CACHE_DURATION`. By default, the cache
duration is set to 5 minutes.

You can manually clear the cache as well and force a reload of all feature
flags by using the `refresh` method:

```php
use CodingPaws\GitLabFeature\Feature;

Feature::refresh();
```

[laravel-cache]: https://laravel.com/docs/8.x/cache
[conf-cache]: https://gitlab.com/codingpaws/gitlab-feature/-/issues/7

## Custom User IDs

By default `Auth::id()` is used to roll out feature flags based on a user’s ID.
You can register a custom User ID resolver if you want to roll out the flags
based on a different ID or the username:

```php
use CodingPaws\GitLabFeature\UserIdResolver;

class MyIdResolver extends UserIdResolver
{
  public function resolve(): int|string|null
  {
    return Auth::user()?->name;
  }
}

UserIdResolver::register(new MyIdResolver);
```

## Viewing all available feature flags

When debugging problems or working with A/B tests, it can be helpful to see the
state of all feature flags. To get a list of all feature flags and whether they
are enabled or not, use the `all` method:

```php
use CodingPaws\GitLabFeature\Feature;

$flags = Feature::all();

echo $flags['cool_feature'] ? 'enabled' : 'disabled';
```

## How to mock feature flags

Mocking of feature flags can be done through [Laravel facade mocking][mock] of
the `Feature` facade:

```php
use CodingPaws\GitLabFeature\Feature;

Feature::shouldReceive('enabled')
   ->with('cool_feature')
   ->once()
   ->andReturn(true);

Feature::enabled('cool_feature'); // returns `true`
```

[mock]: https://laravel.com/docs/8.x/mocking#mocking-facades

## SSL troubleshooting

On certain platforms or depending on your GitLab instance configuration—this
does not apply to the GitLab.com SaaS platform—the in-built SSL verification
can sometimes cause issues and not work. We’ve had problems in development
environments on Windows, even though OpenSSL was installed.

For cases where SSL verification fails, you can disable it by setting the
`gitlab_feature.disable_verification` to `true`. By default, you can set the
`GITLAB_DISABLE_VERIFICATION` environment variable to `true`.
