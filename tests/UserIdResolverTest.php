<?php

namespace Tests;

use CodingPaws\GitLabFeature\UserIdResolver;
use Illuminate\Support\Facades\Auth;

class UserIdResolverTest extends TestCase
{
  public function testDefaultWithoutUser()
  {
    Auth::shouldReceive('id')
      ->andReturn(null);

    $this->assertEquals(null, UserIdResolver::id());
  }

  public function testDefaultWithUser()
  {
    Auth::shouldReceive('id')
      ->andReturn(5);

    $this->assertEquals(5, UserIdResolver::id());
  }

  public function testCustomWithoutUser()
  {
    UserIdResolver::register(new MockUserIdResolver(null));

    $mock = Auth::shouldReceive('id')
      ->never();

    $this->assertEquals(null, UserIdResolver::id());
    $mock->verify();
  }

  public function testCustomWithUser()
  {
    UserIdResolver::register(new MockUserIdResolver('u18793'));

    $mock = Auth::shouldReceive('id')
      ->never();

    $this->assertEquals('u18793', UserIdResolver::id());
    $mock->verify();
  }
}

class MockUserIdResolver extends UserIdResolver
{
  public function __construct(private int|string|null $result)
  {
  }

  public function resolve(): int|string|null
  {
    return $this->result;
  }
}
