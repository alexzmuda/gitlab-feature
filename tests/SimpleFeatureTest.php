<?php

namespace Tests;

use CodingPaws\GitLabFeature\Feature;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class SimpleFeatureTest extends TestCase
{
  public function testEnabledWithoutFlags()
  {
    $mock = Cache::shouldReceive('get')
      ->once()
      ->andReturn([]);

    $this->assertFalse(Feature::enabled('test'));
    $mock->verify();
  }

  public function testDisabledWithoutFlags()
  {
    $mock = Cache::shouldReceive('get')
      ->once()
      ->andReturn([]);
    $this->assertTrue(Feature::disabled('test'));
    $mock->verify();
  }

  public function testEnabled()
  {
    $mock = Cache::shouldReceive('get')
      ->once()
      ->andReturn([[
        "name" => "native_api",
        "description" => "Enables native API for this app",
        "enabled" => true,
        "strategies" => [
          [
            "name" => "default",
            "parameters" => [],
          ],
        ],
      ]]);

    $this->assertTrue(Feature::enabled('native_api'));
    $mock->verify();
  }

  public function testDisabled()
  {
    $mock = Cache::shouldReceive('get')
      ->once()
      ->andReturn([[
        "name" => "native_api",
        "description" => "Enables native API for this app",
        "enabled" => true,
        "strategies" => [
          [
            "name" => "default",
            "parameters" => [],
          ],
        ],
      ]]);

    $this->assertFalse(Feature::disabled('native_api'));
    $mock->verify();
  }

  public function testRefresh()
  {
    $mocks = collect();

    $mocks[] = Config::shouldReceive('get')
      ->times()
      ->with('gitlab_feature.name')
      ->andReturn('test');

    $mocks[] = Cache::shouldReceive('forget')
      ->once()
      ->with('gitlab_feature::features::test')
      ->andReturnNull();

    $mocks[] = Cache::shouldReceive('get')
      ->once()
      ->withSomeOfArgs('gitlab_feature::features::test')
      ->andReturn([1]);

    Feature::refresh();

    $mocks->map->verify();
  }
}
