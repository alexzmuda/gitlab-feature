<?php

namespace Tests\Helpers;

use CodingPaws\GitLabFeature\Base\GitLabLoader;
use CodingPaws\GitLabFeature\Helpers\FeatureFlagService;
use Illuminate\Http\Client\HttpClientException;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class FeatureFlagServiceTest extends TestCase
{
  private string $apiUrl = 'https://example.gitlab.com/api/feature/flags';
  private string $envName = 'production';
  private string $instanceId = 'instance1234';
  private array $exampleFeatureFlags = [1, 2, 3];
  private GitLabLoader $service;

  protected function setUp(): void
  {
    parent::setUp();
    $this->mockConfig('gitlab_feature.name', $this->envName);
    $this->mockConfig('gitlab_feature.instance_id', $this->instanceId);
    $this->mockConfig('gitlab_feature.disable_verification', false);
    $this->service = new FeatureFlagService;
  }

  public function testLoadFromGitLab()
  {
    $this->mockConfig('gitlab_feature.api_url', $this->apiUrl);

    Http::fake(function (Request $request) {
      $this->assertEquals($this->apiUrl . '/client/features', $request->url(), 'URL is incorrect');
      $this->assertEquals($this->envName, $request->header('UNLEASH-APPNAME')[0], 'Env name is incorrect');

      return Http::response(['features' => $this->exampleFeatureFlags], 200);
    });
  }

  public function testRequestsWithCorrectAppName()
  {
    $this->mockConfig('gitlab_feature.api_url', $this->apiUrl);

    Http::fake(function (Request $request) {
      $this->assertEquals($this->apiUrl . '/client/features', $request->url(), 'URL is incorrect');

      return Http::response(['features' => $this->exampleFeatureFlags], 200);
    });
  }

  public function testRequestsWithCorrectInstanceId()
  {
    $this->mockConfig('gitlab_feature.api_url', $this->apiUrl);

    Http::fake(function (Request $request) {
      $this->assertEquals($this->instanceId, $request->header('UNLEASH-INSTANCEID')[0], 'Instance ID is incorrect');

      return Http::response(['features' => $this->exampleFeatureFlags], 200);
    });
  }

  public function testReturnsEmptyArrayForErroringResponses()
  {
    $this->mockConfig('gitlab_feature.api_url', $this->apiUrl);

    Http::fake(function () {
      return Http::response(['features' => $this->exampleFeatureFlags], 500);
    });

    $this->assertEquals([], $this->service->load(), 'Expected no feature flags to be returned when a faulty response is received.');
  }

  public function testExceptionDuringRequestCausesErrorLog()
  {
    $this->mockConfig('gitlab_feature.api_url', $this->apiUrl);

    Http::fake(function () {
      throw new HttpClientException('Example error', 500);
    });

    $this->expectException(HttpClientException::class);
    $this->service->load();
  }

  public function testLoadsFeatureFlagsCorrectly()
  {
    $this->mockConfig('gitlab_feature.api_url', $this->apiUrl);

    Http::fake(Http::response(['features' => $this->exampleFeatureFlags], 200));
    $this->assertEquals($this->exampleFeatureFlags, $this->service->load(), 'Feature flags don’t match mocked flag list');
  }

  public function testAppendsUnleashSuffix()
  {
    $this->mockConfig('gitlab_feature.api_url', $this->apiUrl . '/client/features');

    Http::fake(function (Request $request) {
      $this->assertEquals($this->apiUrl . '/client/features', $request->url(), 'URL is incorrect');

      return Http::response(['features' => $this->exampleFeatureFlags], 200);
    });

    $this->service->load();
  }
}
