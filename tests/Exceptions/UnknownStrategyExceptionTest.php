<?php

namespace Tests\Exceptions;

use CodingPaws\GitLabFeature\Exceptions\UnknownStrategyException;
use PHPUnit\Framework\TestCase;

class UnknownStrategyExceptionTest extends TestCase
{

  public function testMessage()
  {
    $class = "unknown_strategy";

    $exception = new UnknownStrategyException($class);

    $this->assertEquals("unknown_strategy is not a valid strategy.", $exception->getMessage());
  }
}
