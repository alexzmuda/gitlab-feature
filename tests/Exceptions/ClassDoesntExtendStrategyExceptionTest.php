<?php

namespace Tests\Exceptions;

use CodingPaws\GitLabFeature\Exceptions\ClassDoesntExtendStrategyException;
use PHPUnit\Framework\TestCase;

class ClassDoesntExtendStrategyExceptionTest extends TestCase
{
  public function testMessage()
  {
    $class = "Exceptions\\test_class_name";

    $exception = new ClassDoesntExtendStrategyException($class);

    $this->assertEquals("Exceptions\\test_class_name doesn’t extend the CodingPaws\GitLabFeature\Strategies\Base\Strategy class.", $exception->getMessage());
  }
}
