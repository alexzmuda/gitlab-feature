<?php

namespace Tests\Strategies;

use CodingPaws\GitLabFeature\Strategies\Base\Strategy;
use CodingPaws\GitLabFeature\Strategies\DefaultStrategy;
use Tests\TestCase;

class DefaultStrategyTest extends TestCase
{
  public Strategy $strategy;

  protected function setUp(): void
  {
    $this->strategy = new DefaultStrategy();
  }

  public function testName()
  {
    $this->assertEquals('default', $this->strategy->name());
  }

  public function testCheckIsAlwaysTrue()
  {
    $this->assertTrue($this->strategy->check(), 'The default strategy should always be true');
  }
}
