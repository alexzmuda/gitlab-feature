<?php

namespace Tests\Middlewares;

use CodingPaws\GitLabFeature\Feature;
use CodingPaws\GitLabFeature\Middlewares\FeatureFlagMiddleware;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tests\TestCase;

class FeatureFlagMiddlewareTest extends TestCase
{
  public function testWithFlagEnabled()
  {
    $mock = Feature::shouldReceive('enabled')
      ->with('test_flag')
      ->once()
      ->andReturnTrue();

    $middleware = app(FeatureFlagMiddleware::class);
    $result = $middleware->handle(request: new Request(), next: fn () => 1, feature_name: 'test_flag');

    $this->assertEquals(1, $result);
    $mock->verify();
  }

  public function testWithFlagDisabled()
  {
    $mock = Feature::shouldReceive('enabled')
      ->with('test_flag')
      ->once()
      ->andReturnFalse();

    $middleware = app(FeatureFlagMiddleware::class);

    try {
      $middleware->handle(request: new Request(), next: fn () => 1, feature_name: 'test_flag');
      $this->assertFalse('Expected a ' . NotFoundHttpException::class);
    } catch (\Throwable $th) {
      $this->assertEquals(NotFoundHttpException::class, $th::class);
    }

    $mock->verify();
  }
}
