<?php

namespace Tests;

use Carbon\Carbon;
use CodingPaws\GitLabFeature\Base\GitLabLoader;
use CodingPaws\GitLabFeature\Helpers\FeatureFlagCache;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class FeatureFlagCacheTest extends TestCase
{
  public function testGet()
  {
    $feature_flags = [['name' => 'new_design', 'enabled' => false]];

    $mock = Cache::shouldReceive('get')
      ->once()
      ->andReturn($feature_flags);

    $this->assertEquals($feature_flags, FeatureFlagCache::get(null));
    $mock->verify();
  }

  public function testGetWithoutCacheOrService()
  {
    Config::shouldReceive('get')
      ->with('gitlab_feature.name')
      ->once()
      ->andReturn('test_environment');

    Cache::shouldReceive('get')
      ->once()
      ->withAnyArgs()
      ->andReturnUsing(function ($key, $callback) {
        $this->assertEquals(FeatureFlagCache::BASE_CACHE_KEY . "::test_environment", $key);
        return $callback();
      });

    $this->assertEquals([], FeatureFlagCache::get(null));
  }

  public function testGetWithoutCache()
  {
    $mocks = collect();

    $mocks[] = Config::shouldReceive('get')
      ->with('gitlab_feature.name')
      ->twice()
      ->andReturn('test_environment');

    $mocks[] = Cache::shouldReceive('get')
      ->once()
      ->withAnyArgs()
      ->andReturnUsing(function ($key, $callback) {
        $this->assertEquals(FeatureFlagCache::BASE_CACHE_KEY . "::test_environment", $key);
        return $callback();
      });

    $mocks[] = Config::shouldReceive('get')
      ->with('gitlab_feature.cache_duration')
      ->andReturn(15);

    $loader = new TestLoader;

    $mocks[] = Cache::shouldReceive('put')
      ->once()
      ->withSomeOfArgs(FeatureFlagCache::BASE_CACHE_KEY . "::test_environment", $loader->flags);

    $this->assertEquals($loader->flags, FeatureFlagCache::get($loader));
    $mocks->map->verify();
  }

  public function testClearCache()
  {
    $mocks = collect();

    $mocks[] = Config::shouldReceive('get')
      ->with('gitlab_feature.name')
      ->once()
      ->andReturn('test_environment');

    $mocks[] = Cache::shouldReceive('forget')
      ->once()
      ->with(FeatureFlagCache::BASE_CACHE_KEY . "::test_environment")
      ->andReturnUsing(fn () => $this->assertTrue(true));

    FeatureFlagCache::clear();
    $mocks->map->verify();
  }
}

class TestLoader implements GitLabLoader
{
  public array $flags = [['name' => 'new_design', 'enabled' => false]];

  public function load(): array
  {
    return $this->flags;
  }
}
