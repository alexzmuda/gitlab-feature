<?php

namespace CodingPaws\GitLabFeature\Strategies;

use CodingPaws\GitLabFeature\Strategies\Base\Strategy;

class DefaultStrategy extends Strategy
{
  public function name(): string
  {
    return 'default';
  }

  public function check(): bool
  {
    return true;
  }
}
