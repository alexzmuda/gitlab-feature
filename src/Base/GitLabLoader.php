<?php

namespace CodingPaws\GitLabFeature\Base;

interface GitLabLoader
{
  /**
   * Load feature flags from GitLab
   */
  public function load(): array;
}
