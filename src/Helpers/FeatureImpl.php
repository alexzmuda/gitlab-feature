<?php

namespace CodingPaws\GitLabFeature\Helpers;

use CodingPaws\GitLabFeature\Traits\ChecksStrategy;

class FeatureImpl
{
  use ChecksStrategy;

  private $featureFlags = null;

  public function __construct(
    private FeatureFlagService $service
  ) {
  }

  public function all(): array
  {
    $this->checkFeatures();

    $flags = [];

    foreach ($this->featureFlags as $flag) {
      $enabled = $flag['enabled'];
      $enabled = $enabled && $this->checkStrategies($flag['strategies']);

      $flags[$flag['name']] = $enabled;
    }

    return $flags;
  }

  public function enabled(string $name, bool $default = false): bool
  {
    $this->checkFeatures();

    $feature = $this->getFeature($name);

    if (!$feature) {
      return $default;
    }

    return $feature['enabled'] ? $this->checkStrategies($feature['strategies']) : false;
  }

  public function disabled(string $name, bool $default = false)
  {
    return !$this->enabled($name, $default);
  }

  public function refresh(): void
  {
    FeatureFlagCache::clear();
    $this->checkFeatures();
  }

  private function getFeature(string $name): ?array
  {
    if (!$name) {
      return null;
    }

    foreach ($this->featureFlags as $feature) {
      if (strtolower($feature['name']) === strtolower($name)) {
        return $feature;
      }
    }

    return null;
  }

  private function checkFeatures(): void
  {
    if (!is_array($this->featureFlags) || defined('CODINGPAWS_TESTS_RUNNING')) {
      $this->featureFlags = FeatureFlagCache::get($this->service);
    }
  }
}
