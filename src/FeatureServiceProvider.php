<?php

namespace CodingPaws\GitLabFeature;

use CodingPaws\GitLabFeature\Helpers\FeatureFlagService;
use CodingPaws\GitLabFeature\Helpers\FeatureImpl;
use CodingPaws\GitLabFeature\Middlewares\FeatureFlagMiddleware;
use CodingPaws\GitLabFeature\Strategies\Base\StrategyResolver;
use CodingPaws\GitLabFeature\Strategies\DefaultStrategy;
use CodingPaws\GitLabFeature\Strategies\FlexibleRolloutStrategy;
use CodingPaws\GitLabFeature\Strategies\IdStrategy;
use CodingPaws\GitLabFeature\Strategies\UserPercentageStrategy;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class FeatureServiceProvider extends ServiceProvider
{
  public function boot(): void
  {
    $this->publishes([
      __DIR__ . '/../config/gitlab_feature.php' => $this->app->configPath('gitlab_feature.php'),
    ]);
    $router = $this->app->make(Router::class);
    $router->aliasMiddleware('feature_flag', FeatureFlagMiddleware::class);
  }

  public function register(): void
  {
    $this->mergeConfigFrom(__DIR__ . '/../config/gitlab_feature.php', 'gitlab_feature');

    StrategyResolver::register(DefaultStrategy::class);
    StrategyResolver::register(IdStrategy::class);
    StrategyResolver::register(UserPercentageStrategy::class);
    StrategyResolver::register(FlexibleRolloutStrategy::class);

    $this->app->bind('gitlab_feature', static function () {
      return new FeatureImpl(new FeatureFlagService());
    });
  }
}
